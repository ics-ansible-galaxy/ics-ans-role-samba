# ics-ans-role-samba

Ansible role to install samba.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
this role install samba and have some specific cutomization if the domain name is PSS

it can download some windows packages file from artifactory to allow PSS windows Dev. machine to install these software using the pss NAS server:
pss_nas_software_repo_url: https://artifactory.esss.lu.se/artifactory/list/PSS-Softwares/
pss_nas_software_files:
  - test.zip



```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-samba
```

## License

BSD 2-clause
